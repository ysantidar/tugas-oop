<?php
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');
    $sheep=new Animal("Shaun");
    echo "Name : ".$sheep->name."<br>";
    echo "Legs : ".$sheep->legs."<br>";
    echo "Cold_Blooded : ".$sheep->cold_blooded."<br> <br>";

    $frog = new Frog("Buduk");
    echo "Name : ".$frog->name."<br>";
    echo "Legs : ".$frog->legs."<br>";
    echo "Cold_Blooded : ".$frog->cold_blooded."<br>";
    echo "Jump : ".$frog->jump . "<br> <br>";

    $ape = new Ape("Kera Sakti");
    echo "Name : ".$ape->name."<br>";
    echo "Legs : ".$ape->legs."<br>";
    echo "Cold_Blooded : ".$ape->cold_blooded."<br>";
    echo "Jump : ".$ape->yell . "<br><br>";


?>